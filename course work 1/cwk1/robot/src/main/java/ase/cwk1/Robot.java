// COMP5911M Coursework 1, Task 1

package ase.cwk1;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import java.io.PrintWriter;
import java.util.ArrayList;


public class Robot {
  private Machine location;
  private String item;

  public Machine getLocation() {
    return location;
  }
  

  public void moveTo(Machine machine) {
    location = machine;
  }

  public String getItem() {
    return item;
  }
  
  public void printinforobo(Robot robot){

    if (robot.getLocation() != null) {
      output.write(" location=" + robot.getLocation().getName());
    }
    if (robot.getItem() != null) {
      output.write(" item=" + robot.getItem());
    }
  }

  public void pick() {
    item = location.removeItem();
  }

  public void release() {
    location.addItem(item);
    item = null;
  }
}
