// COMP5911M Coursework 1, Task 1

package ase.cwk1;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import java.io.PrintWriter;
import java.util.ArrayList;


public class Machine  {
  private String name;
  private String location;
  private String item;
  private Writer output;

  public Machine(String name, String location) {
    this.name = name;
    this.location = location;
  }

  public String getName() {
    return name;
  }
// code moved from report class to counter long class bad smell

  public void printinfo (List<Machine> machines){


  for (Machine machine: machines) {
      output.write("Machine " + machine.getName());
      if (machine.getItem() != null) {
        output.write(" item=" + machine.getItem());
      }


  }}

  public String getLocation() {
    return location;
  }

  public String getItem() {
    return item;
  }

  public void addItem(String item) {
    this.item = item;
  }

  public String removeItem() {
    String removed = item;
    item = null;
    return removed;
  }
}
