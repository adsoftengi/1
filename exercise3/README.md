# Unit Testing With JUnit 5

This exercise provides you with a class `Money`, and an incomplete set of
unit tests for that class, in a separate class named `MoneyTest`.  You will
find the code for these under `src/main` and `src/test`, respectively.

If you are using Linux, a Mac or Windows Subsystem for Linux, you can
run the tests using JUnit's `ConsoleLauncher`; cd into `console` and examine
the README in that directory for further details.

The recommended way of running the tests, which should work on any platform,
is to use the provided **Gradle wrapper**.  On Linux or macOS, do

    ./gradlew test

On Windows, use the same command but omit the `./` at the start.

If the command fails on Linux or macOS, it's likely that the Gradle wrapper
script doesn't have execute permission.  To fix this, do `chmod u+x gradlew`.

**Note**: the first time you run the tests, it will be very slow, as the
Gradle wrapper will need to download the Gradle build system and the required
unit testing libraries.  Subsequent runs will be a lot faster.

Note that Gradle won't normally rerun tests if they have all passed, unless
you've changed something in your code.  You can force it to recompile
everything and rerun the tests using

    ./gradlew test --rerun-tasks

You can remove all files generated by the build process with

    ./gradlew clean
